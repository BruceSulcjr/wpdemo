<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wpdemo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?K)N9&!Y/5m|=mbU/p`N5vXW1<Et#?*l%d*5r($N~<-O9F |+W!rU$GY$|IeqNJY');
define('SECURE_AUTH_KEY',  'y7h.tlY>:o}=ARk$lma79tAZ1vCj>O)(NNkvwpr%XO?ye5]5+P^TF+9nw.(;8&f3');
define('LOGGED_IN_KEY',    'gL%(k;*+)Um0q{o(Wjq^_HU-k/YbLzSGru-<t@]g:k~v8ol,|%[Ig>Y;,g% J&B|');
define('NONCE_KEY',        '^p<zo_2#j8^cpC;pYCJ-*YLKyl-JRM|?*HKhx{BlmX6kzCLB0`mgp=?gD!u8!y<<');
define('AUTH_SALT',        '-ZB+T?r;S72=~+$tlw!y){}w8mK>X{cupB($|hkaAib/;AHgu2HV&Cmao+I8X[jz');
define('SECURE_AUTH_SALT', 'rDeqCVT&TU/ScqsCC*{+,H9v%++m#?S+`>lI)vLVTK_G0_0LC+-{8dL;LW-;>7a#');
define('LOGGED_IN_SALT',   'Hgp0ry?K(YW~5U_5G5Z%lr[dtB=^q,Y}mxWe!4R>qennq$[HEHk`SP0AE_`BUUh?');
define('NONCE_SALT',       '(lK;@M9g1gS*ZT`Z#k~&RKaOu1*M=+-L<E!eUK{[=qz- Lq9VM@VwJ`]Z`MuC@GK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
